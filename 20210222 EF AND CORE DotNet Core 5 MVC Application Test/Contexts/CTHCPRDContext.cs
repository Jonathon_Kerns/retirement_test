﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Contexts
{
    public partial class CTHCPRDContext : DbContext
    {
        public CTHCPRDContext()
        {
        }

        public CTHCPRDContext(DbContextOptions<CTHCPRDContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CotAdjReason> CotAdjReasons { get; set; }
        public virtual DbSet<Escal> Escals { get; set; }
        public virtual DbSet<InputAndException> InputAndExceptions { get; set; }
        public virtual DbSet<PsCotPensionTbl> PsCotPensionTbls { get; set; }
        public virtual DbSet<PsEarningsTbl> PsEarningsTbls { get; set; }
        public virtual DbSet<PsPayCheck> PsPayChecks { get; set; }
        public virtual DbSet<PsPayOthEarn> PsPayOthEarns { get; set; }
        public virtual DbSet<SalaryDatum> SalaryData { get; set; }
        public virtual DbSet<SalaryHistory> SalaryHistories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=127.0.0.1,14333;Initial Catalog=CTHCPRD;Persist Security Info=True;User ID=sa;Password=Welcome1!");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<CotAdjReason>(entity =>
            {
                entity.ToView("CotAdjReason");

                entity.Property(e => e.CotAdjReason1).IsUnicode(false);

                entity.Property(e => e.Emplid).IsUnicode(false);
            });

            modelBuilder.Entity<Escal>(entity =>
            {
                entity.ToView("Escal");
            });

            modelBuilder.Entity<InputAndException>(entity =>
            {
                entity.ToView("InputAndExceptions");

                entity.Property(e => e.Descr).IsUnicode(false);

                entity.Property(e => e.EarnCode).IsUnicode(false);

                entity.Property(e => e.Emplid).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.YrMth).IsUnicode(false);
            });

            modelBuilder.Entity<PsCotPensionTbl>(entity =>
            {
                entity.Property(e => e.CotAdjReason).IsUnicode(false);

                entity.Property(e => e.CotBuybackCd).IsUnicode(false);

                entity.Property(e => e.CotOptSelected).IsUnicode(false);

                entity.Property(e => e.Emplid).IsUnicode(false);

                entity.Property(e => e.PriorService).IsUnicode(false);

                entity.Property(e => e.RetireFlag).IsUnicode(false);

                entity.Property(e => e.Runcntlid).IsUnicode(false);
            });

            modelBuilder.Entity<PsEarningsTbl>(entity =>
            {
                entity.Property(e => e.AddDe).IsUnicode(false);

                entity.Property(e => e.AddGross).IsUnicode(false);

                entity.Property(e => e.AllowEmpltype).IsUnicode(false);

                entity.Property(e => e.AmtOrHours).IsUnicode(false);

                entity.Property(e => e.BasedOnAccErncd).IsUnicode(false);

                entity.Property(e => e.BasedOnErncd).IsUnicode(false);

                entity.Property(e => e.BasedOnType).IsUnicode(false);

                entity.Property(e => e.BudgetEffect).IsUnicode(false);

                entity.Property(e => e.DedcdPayback).IsUnicode(false);

                entity.Property(e => e.Descr).IsUnicode(false);

                entity.Property(e => e.Descrshort).IsUnicode(false);

                entity.Property(e => e.EffStatus).IsUnicode(false);

                entity.Property(e => e.EffectOnFlsa).IsUnicode(false);

                entity.Property(e => e.EligForRetropay).IsUnicode(false);

                entity.Property(e => e.Erncd).IsUnicode(false);

                entity.Property(e => e.FlsaCategory).IsUnicode(false);

                entity.Property(e => e.GlExpense).IsUnicode(false);

                entity.Property(e => e.GvtAddToSf5052).IsUnicode(false);

                entity.Property(e => e.GvtBenefitsRate).IsUnicode(false);

                entity.Property(e => e.GvtCpdfErncd).IsUnicode(false);

                entity.Property(e => e.GvtFeffla).IsUnicode(false);

                entity.Property(e => e.GvtFmla).IsUnicode(false);

                entity.Property(e => e.GvtIncludeLoc).IsUnicode(false);

                entity.Property(e => e.GvtIrrLwop).IsUnicode(false);

                entity.Property(e => e.GvtIrrReportable).IsUnicode(false);

                entity.Property(e => e.GvtLvEarnType).IsUnicode(false);

                entity.Property(e => e.GvtOtPayInd).IsUnicode(false);

                entity.Property(e => e.GvtOthPay).IsUnicode(false);

                entity.Property(e => e.GvtPayCap).IsUnicode(false);

                entity.Property(e => e.GvtPremPay).IsUnicode(false);

                entity.Property(e => e.GvtSf113aLumpsum).IsUnicode(false);

                entity.Property(e => e.GvtSf113aWages).IsUnicode(false);

                entity.Property(e => e.HpAdminstipFlag).IsUnicode(false);

                entity.Property(e => e.HrsDistSw).IsUnicode(false);

                entity.Property(e => e.HrsOnly).IsUnicode(false);

                entity.Property(e => e.IncomeCd1042).IsUnicode(false);

                entity.Property(e => e.MaintainBalances).IsUnicode(false);

                entity.Property(e => e.PaymentType).IsUnicode(false);

                entity.Property(e => e.PermanencyNld).IsUnicode(false);

                entity.Property(e => e.PnaUseSglEmpl).IsUnicode(false);

                entity.Property(e => e.RegPayIncluded).IsUnicode(false);

                entity.Property(e => e.ShiftDiffElig).IsUnicode(false);

                entity.Property(e => e.SpecCalcRtn).IsUnicode(false);

                entity.Property(e => e.SubjectCit).IsUnicode(false);

                entity.Property(e => e.SubjectCpp).IsUnicode(false);

                entity.Property(e => e.SubjectCui).IsUnicode(false);

                entity.Property(e => e.SubjectCuiHours).IsUnicode(false);

                entity.Property(e => e.SubjectFica).IsUnicode(false);

                entity.Property(e => e.SubjectFut).IsUnicode(false);

                entity.Property(e => e.SubjectFwt).IsUnicode(false);

                entity.Property(e => e.SubjectPayTax).IsUnicode(false);

                entity.Property(e => e.SubjectQit).IsUnicode(false);

                entity.Property(e => e.SubjectQpip).IsUnicode(false);

                entity.Property(e => e.SubjectQpp).IsUnicode(false);

                entity.Property(e => e.SubjectReg).IsUnicode(false);

                entity.Property(e => e.SubjectRv2).IsUnicode(false);

                entity.Property(e => e.SubjectT4a).IsUnicode(false);

                entity.Property(e => e.SubjectTrueRvgrs).IsUnicode(false);

                entity.Property(e => e.SubjectTrueT4grs).IsUnicode(false);

                entity.Property(e => e.SubtractEarns).IsUnicode(false);

                entity.Property(e => e.TaxClassNld).IsUnicode(false);

                entity.Property(e => e.TaxGrsCompnt).IsUnicode(false);

                entity.Property(e => e.TaxMethod).IsUnicode(false);

                entity.Property(e => e.TipsCategory).IsUnicode(false);

                entity.Property(e => e.UsedToPayRetro).IsUnicode(false);

                entity.Property(e => e.WithholdFwt).IsUnicode(false);
            });

            modelBuilder.Entity<PsPayCheck>(entity =>
            {
                entity.Property(e => e.AddrField1).IsUnicode(false);

                entity.Property(e => e.AddrField2).IsUnicode(false);

                entity.Property(e => e.AddrField3).IsUnicode(false);

                entity.Property(e => e.Address1).IsUnicode(false);

                entity.Property(e => e.Address2).IsUnicode(false);

                entity.Property(e => e.Address3).IsUnicode(false);

                entity.Property(e => e.Address4).IsUnicode(false);

                entity.Property(e => e.BusinessUnit).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.Company).IsUnicode(false);

                entity.Property(e => e.Country).IsUnicode(false);

                entity.Property(e => e.County).IsUnicode(false);

                entity.Property(e => e.Deptid).IsUnicode(false);

                entity.Property(e => e.EmplType).IsUnicode(false);

                entity.Property(e => e.Emplid).IsUnicode(false);

                entity.Property(e => e.FormId).IsUnicode(false);

                entity.Property(e => e.GeoCode).IsUnicode(false);

                entity.Property(e => e.GvtEcsLtdIndic).IsUnicode(false);

                entity.Property(e => e.GvtEcsOffAcct).IsUnicode(false);

                entity.Property(e => e.GvtPayIdLine1).IsUnicode(false);

                entity.Property(e => e.GvtPayIdLine2).IsUnicode(false);

                entity.Property(e => e.GvtScheduleNo).IsUnicode(false);

                entity.Property(e => e.HouseType).IsUnicode(false);

                entity.Property(e => e.HpCorrectStatus).IsUnicode(false);

                entity.Property(e => e.HrWpProcessFlg).IsUnicode(false);

                entity.Property(e => e.InCityLimit).IsUnicode(false);

                entity.Property(e => e.Location).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Num1).IsUnicode(false);

                entity.Property(e => e.Num2).IsUnicode(false);

                entity.Property(e => e.OffCycle).IsUnicode(false);

                entity.Property(e => e.PaySheetSrc).IsUnicode(false);

                entity.Property(e => e.PaycheckAddrOptn).IsUnicode(false);

                entity.Property(e => e.PaycheckAdjust).IsUnicode(false);

                entity.Property(e => e.PaycheckCashed).IsUnicode(false);

                entity.Property(e => e.PaycheckDistKey1).IsUnicode(false);

                entity.Property(e => e.PaycheckDistKey2).IsUnicode(false);

                entity.Property(e => e.PaycheckName).IsUnicode(false);

                entity.Property(e => e.PaycheckOption).IsUnicode(false);

                entity.Property(e => e.PaycheckReprint).IsUnicode(false);

                entity.Property(e => e.PaycheckStatus).IsUnicode(false);

                entity.Property(e => e.Paygroup).IsUnicode(false);

                entity.Property(e => e.Postal).IsUnicode(false);

                entity.Property(e => e.Province).IsUnicode(false);

                entity.Property(e => e.Sin).IsUnicode(false);

                entity.Property(e => e.Ssn).IsUnicode(false);

                entity.Property(e => e.State).IsUnicode(false);
            });

            modelBuilder.Entity<PsPayOthEarn>(entity =>
            {
                entity.Property(e => e.AddGross).IsUnicode(false);

                entity.Property(e => e.Applid).IsUnicode(false);

                entity.Property(e => e.BasCreditSw).IsUnicode(false);

                entity.Property(e => e.CompRatecd).IsUnicode(false);

                entity.Property(e => e.Company).IsUnicode(false);

                entity.Property(e => e.CurrencyCd).IsUnicode(false);

                entity.Property(e => e.EimKey).IsUnicode(false);

                entity.Property(e => e.Erncd).IsUnicode(false);

                entity.Property(e => e.ExDocId).IsUnicode(false);

                entity.Property(e => e.ExDocType).IsUnicode(false);

                entity.Property(e => e.GbGroupId).IsUnicode(false);

                entity.Property(e => e.HrsDistSw).IsUnicode(false);

                entity.Property(e => e.JobPay).IsUnicode(false);

                entity.Property(e => e.NotifyStatus).IsUnicode(false);

                entity.Property(e => e.OffCycle).IsUnicode(false);

                entity.Property(e => e.Paygroup).IsUnicode(false);

                entity.Property(e => e.PuSource).IsUnicode(false);

                entity.Property(e => e.RateUsed).IsUnicode(false);

                entity.Property(e => e.RetropaySeqNo).IsUnicode(false);

                entity.Property(e => e.TaxMethod).IsUnicode(false);

                entity.Property(e => e.TlSource).IsUnicode(false);

                entity.Property(e => e.VcPayoutPrdId).IsUnicode(false);

                entity.Property(e => e.VcPlanId).IsUnicode(false);
            });

            modelBuilder.Entity<SalaryDatum>(entity =>
            {
                entity.ToView("SalaryData");

                entity.Property(e => e.EffectiveDate).IsUnicode(false);

                entity.Property(e => e.Emplid).IsUnicode(false);

                entity.Property(e => e.Month).IsUnicode(false);

                entity.Property(e => e.Year).IsUnicode(false);

                entity.Property(e => e.YearTotal).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    [Table("PS_PAY_CHECK")]
    public partial class PsPayCheck
    {
        [Required]
        [Column("COMPANY")]
        [StringLength(3)]
        public string Company { get; set; }
        [Required]
        [Column("PAYGROUP")]
        [StringLength(3)]
        public string Paygroup { get; set; }
        [Column("PAY_END_DT", TypeName = "datetime")]
        public DateTime PayEndDt { get; set; }
        [Required]
        [Column("OFF_CYCLE")]
        [StringLength(1)]
        public string OffCycle { get; set; }
        [Column("PAGE_NUM")]
        public int PageNum { get; set; }
        [Column("LINE_NUM")]
        public short LineNum { get; set; }
        [Column("SEPCHK")]
        public short Sepchk { get; set; }
        [Required]
        [Column("FORM_ID")]
        [StringLength(6)]
        public string FormId { get; set; }
        [Column("PAYCHECK_NBR", TypeName = "decimal(15, 0)")]
        public decimal PaycheckNbr { get; set; }
        [Required]
        [Column("EMPLID")]
        [StringLength(11)]
        public string Emplid { get; set; }
        [Column("EMPL_RCD")]
        public short EmplRcd { get; set; }
        [Required]
        [Column("NAME")]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [Column("DEPTID")]
        [StringLength(10)]
        public string Deptid { get; set; }
        [Required]
        [Column("EMPL_TYPE")]
        [StringLength(1)]
        public string EmplType { get; set; }
        [Required]
        [Column("SSN")]
        [StringLength(9)]
        public string Ssn { get; set; }
        [Required]
        [Column("SIN")]
        [StringLength(9)]
        public string Sin { get; set; }
        [Column("TOTAL_GROSS", TypeName = "decimal(10, 2)")]
        public decimal TotalGross { get; set; }
        [Column("TOTAL_TAXES", TypeName = "decimal(10, 2)")]
        public decimal TotalTaxes { get; set; }
        [Column("TOTAL_DEDUCTIONS", TypeName = "decimal(10, 2)")]
        public decimal TotalDeductions { get; set; }
        [Column("NET_PAY", TypeName = "decimal(10, 2)")]
        public decimal NetPay { get; set; }
        [Column("CHECK_DT", TypeName = "datetime")]
        public DateTime? CheckDt { get; set; }
        [Required]
        [Column("PAYCHECK_STATUS")]
        [StringLength(1)]
        public string PaycheckStatus { get; set; }
        [Required]
        [Column("PAYCHECK_OPTION")]
        [StringLength(1)]
        public string PaycheckOption { get; set; }
        [Required]
        [Column("PAYCHECK_ADJUST")]
        [StringLength(1)]
        public string PaycheckAdjust { get; set; }
        [Required]
        [Column("PAYCHECK_REPRINT")]
        [StringLength(1)]
        public string PaycheckReprint { get; set; }
        [Required]
        [Column("PAYCHECK_CASHED")]
        [StringLength(1)]
        public string PaycheckCashed { get; set; }
        [Required]
        [Column("PAYCHECK_ADDR_OPTN")]
        [StringLength(1)]
        public string PaycheckAddrOptn { get; set; }
        [Required]
        [Column("PAYCHECK_NAME")]
        [StringLength(50)]
        public string PaycheckName { get; set; }
        [Required]
        [Column("COUNTRY")]
        [StringLength(3)]
        public string Country { get; set; }
        [Required]
        [Column("ADDRESS1")]
        [StringLength(55)]
        public string Address1 { get; set; }
        [Required]
        [Column("ADDRESS2")]
        [StringLength(55)]
        public string Address2 { get; set; }
        [Required]
        [Column("ADDRESS3")]
        [StringLength(55)]
        public string Address3 { get; set; }
        [Required]
        [Column("ADDRESS4")]
        [StringLength(55)]
        public string Address4 { get; set; }
        [Required]
        [Column("CITY")]
        [StringLength(30)]
        public string City { get; set; }
        [Required]
        [Column("NUM1")]
        [StringLength(6)]
        public string Num1 { get; set; }
        [Required]
        [Column("NUM2")]
        [StringLength(6)]
        public string Num2 { get; set; }
        [Required]
        [Column("HOUSE_TYPE")]
        [StringLength(2)]
        public string HouseType { get; set; }
        [Required]
        [Column("ADDR_FIELD1")]
        [StringLength(2)]
        public string AddrField1 { get; set; }
        [Required]
        [Column("ADDR_FIELD2")]
        [StringLength(4)]
        public string AddrField2 { get; set; }
        [Required]
        [Column("ADDR_FIELD3")]
        [StringLength(4)]
        public string AddrField3 { get; set; }
        [Required]
        [Column("COUNTY")]
        [StringLength(30)]
        public string County { get; set; }
        [Required]
        [Column("STATE")]
        [StringLength(6)]
        public string State { get; set; }
        [Required]
        [Column("POSTAL")]
        [StringLength(12)]
        public string Postal { get; set; }
        [Required]
        [Column("GEO_CODE")]
        [StringLength(11)]
        public string GeoCode { get; set; }
        [Required]
        [Column("IN_CITY_LIMIT")]
        [StringLength(1)]
        public string InCityLimit { get; set; }
        [Required]
        [Column("LOCATION")]
        [StringLength(10)]
        public string Location { get; set; }
        [Required]
        [Column("PAYCHECK_DIST_KEY1")]
        [StringLength(90)]
        public string PaycheckDistKey1 { get; set; }
        [Required]
        [Column("PAYCHECK_DIST_KEY2")]
        [StringLength(90)]
        public string PaycheckDistKey2 { get; set; }
        [Column("BENEFIT_RCD_NBR")]
        public short BenefitRcdNbr { get; set; }
        [Required]
        [Column("PAY_SHEET_SRC")]
        [StringLength(1)]
        public string PaySheetSrc { get; set; }
        [Required]
        [Column("BUSINESS_UNIT")]
        [StringLength(5)]
        public string BusinessUnit { get; set; }
        [Required]
        [Column("GVT_SCHEDULE_NO")]
        [StringLength(14)]
        public string GvtScheduleNo { get; set; }
        [Required]
        [Column("GVT_PAY_ID_LINE_1")]
        [StringLength(40)]
        public string GvtPayIdLine1 { get; set; }
        [Required]
        [Column("GVT_PAY_ID_LINE_2")]
        [StringLength(40)]
        public string GvtPayIdLine2 { get; set; }
        [Required]
        [Column("GVT_ECS_LTD_INDIC")]
        [StringLength(1)]
        public string GvtEcsLtdIndic { get; set; }
        [Required]
        [Column("GVT_ECS_OFF_ACCT")]
        [StringLength(16)]
        public string GvtEcsOffAcct { get; set; }
        [Column("GVT_RITS_DT", TypeName = "datetime")]
        public DateTime? GvtRitsDt { get; set; }
        [Column("GVT_TSP_SEQ_YR")]
        public short GvtTspSeqYr { get; set; }
        [Column("GVT_TSP_SEQ_NO")]
        public short GvtTspSeqNo { get; set; }
        [Required]
        [Column("PROVINCE")]
        [StringLength(6)]
        public string Province { get; set; }
        [Required]
        [Column("HP_CORRECT_STATUS")]
        [StringLength(1)]
        public string HpCorrectStatus { get; set; }
        [Column("HP_CORRECTED_DT", TypeName = "datetime")]
        public DateTime? HpCorrectedDt { get; set; }
        [Required]
        [Column("HR_WP_PROCESS_FLG")]
        [StringLength(1)]
        public string HrWpProcessFlg { get; set; }
        [Column("UPDATE_DT", TypeName = "datetime")]
        public DateTime? UpdateDt { get; set; }
    }
}

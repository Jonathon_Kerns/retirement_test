﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{

    [Keyless]
    public partial class SalaryDatum
    {
        [Required]
        [Column("EMPLID")]
        [StringLength(11)]
        public string Emplid { get; set; }
        [StringLength(4)]
        public string Year { get; set; }
        [StringLength(2)]
        public string Month { get; set; }
        [Column("Total_SSP", TypeName = "decimal(38, 2)")]
        public decimal? TotalSsp { get; set; }
        [Required]
        [StringLength(3)]
        public string YearTotal { get; set; }
        [Column("Gen_SSP", TypeName = "decimal(38, 2)")]
        public decimal? GenSsp { get; set; }
        [Column("Fire_SSP", TypeName = "decimal(38, 2)")]
        public decimal? FireSsp { get; set; }
        [Column("Police_SSP", TypeName = "decimal(38, 2)")]
        public decimal? PoliceSsp { get; set; }
        [Column("Effective_Date")]
        [StringLength(6)]
        public string EffectiveDate { get; set; }

        public DateTime EFFDT { get; set; }
    }
}

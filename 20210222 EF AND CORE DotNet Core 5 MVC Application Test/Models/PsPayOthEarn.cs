﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    [Table("PS_PAY_OTH_EARNS")]
    public partial class PsPayOthEarn
    {
        [Required]
        [Column("COMPANY")]
        [StringLength(3)]
        public string Company { get; set; }
        [Required]
        [Column("PAYGROUP")]
        [StringLength(3)]
        public string Paygroup { get; set; }
        [Column("PAY_END_DT", TypeName = "datetime")]
        public DateTime PayEndDt { get; set; }
        [Required]
        [Column("OFF_CYCLE")]
        [StringLength(1)]
        public string OffCycle { get; set; }
        [Column("PAGE_NUM")]
        public int PageNum { get; set; }
        [Column("LINE_NUM")]
        public short LineNum { get; set; }
        [Column("ADDL_NBR")]
        public short AddlNbr { get; set; }
        [Required]
        [Column("ERNCD")]
        [StringLength(3)]
        public string Erncd { get; set; }
        [Required]
        [Column("RATE_USED")]
        [StringLength(1)]
        public string RateUsed { get; set; }
        [Column("SEPCHK")]
        public short Sepchk { get; set; }
        [Required]
        [Column("JOB_PAY")]
        [StringLength(1)]
        public string JobPay { get; set; }
        [Column("OTH_HRS", TypeName = "decimal(6, 2)")]
        public decimal OthHrs { get; set; }
        [Column("OTH_PAY", TypeName = "decimal(10, 2)")]
        public decimal OthPay { get; set; }
        [Column("OTH_EARNS", TypeName = "decimal(10, 2)")]
        public decimal OthEarns { get; set; }
        [Required]
        [Column("ADD_GROSS")]
        [StringLength(1)]
        public string AddGross { get; set; }
        [Required]
        [Column("TAX_METHOD")]
        [StringLength(1)]
        public string TaxMethod { get; set; }
        [Column("ADDL_SEQ")]
        public short AddlSeq { get; set; }
        [Required]
        [Column("TL_SOURCE")]
        [StringLength(1)]
        public string TlSource { get; set; }
        [Required]
        [Column("BAS_CREDIT_SW")]
        [StringLength(1)]
        public string BasCreditSw { get; set; }
        [Required]
        [Column("COMP_RATECD")]
        [StringLength(6)]
        public string CompRatecd { get; set; }
        [Column("COMPRATE", TypeName = "decimal(18, 6)")]
        public decimal Comprate { get; set; }
        [Column("COMPRATE_USED", TypeName = "decimal(18, 6)")]
        public decimal ComprateUsed { get; set; }
        [Required]
        [Column("HRS_DIST_SW")]
        [StringLength(1)]
        public string HrsDistSw { get; set; }
        [Column("XREF_NUM", TypeName = "decimal(15, 0)")]
        public decimal XrefNum { get; set; }
        [Required]
        [Column("EX_DOC_ID")]
        [StringLength(10)]
        public string ExDocId { get; set; }
        [Required]
        [Column("EX_DOC_TYPE")]
        [StringLength(1)]
        public string ExDocType { get; set; }
        [Column("EX_LINE_NBR")]
        public int ExLineNbr { get; set; }
        [Required]
        [Column("CURRENCY_CD")]
        [StringLength(3)]
        public string CurrencyCd { get; set; }
        [Required]
        [Column("VC_PLAN_ID")]
        [StringLength(10)]
        public string VcPlanId { get; set; }
        [Required]
        [Column("VC_PAYOUT_PRD_ID")]
        [StringLength(10)]
        public string VcPayoutPrdId { get; set; }
        [Required]
        [Column("GB_GROUP_ID")]
        [StringLength(15)]
        public string GbGroupId { get; set; }
        [Required]
        [Column("APPLID")]
        [StringLength(11)]
        public string Applid { get; set; }
        [Column("AWARD_DATE", TypeName = "datetime")]
        public DateTime? AwardDate { get; set; }
        [Required]
        [Column("NOTIFY_STATUS")]
        [StringLength(1)]
        public string NotifyStatus { get; set; }
        [Required]
        [Column("EIM_KEY")]
        [StringLength(20)]
        public string EimKey { get; set; }
        [Required]
        [Column("PU_SOURCE")]
        [StringLength(2)]
        public string PuSource { get; set; }
        [Required]
        [Column("RETROPAY_SEQ_NO")]
        [StringLength(15)]
        public string RetropaySeqNo { get; set; }
    }
}

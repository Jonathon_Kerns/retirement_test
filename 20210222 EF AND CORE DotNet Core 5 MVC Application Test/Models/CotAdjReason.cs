﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    public partial class CotAdjReason
    {
        [Required]
        [Column("EMPLID")]
        [StringLength(11)]
        public string Emplid { get; set; }
        [Column("EFFDT", TypeName = "datetime")]
        public DateTime Effdt { get; set; }
        [Required]
        [Column("COT_ADJ_REASON")]
        [StringLength(100)]
        public string CotAdjReason1 { get; set; }
    }
}

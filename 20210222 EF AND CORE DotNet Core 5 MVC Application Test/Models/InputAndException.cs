﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    public partial class InputAndException
    {
        [Required]
        [Column("EMPLID")]
        [StringLength(11)]
        public string Emplid { get; set; }
        [StringLength(30)]
        public string Name { get; set; }
        [Required]
        [Column("Earn_Code")]
        [StringLength(3)]
        public string EarnCode { get; set; }
        [Required]
        [Column("DESCR")]
        [StringLength(30)]
        public string Descr { get; set; }
        [StringLength(6)]
        public string YrMth { get; set; }
        [Column(TypeName = "decimal(38, 2)")]
        public decimal? Earnings { get; set; }
        [Column(TypeName = "decimal(38, 2)")]
        public decimal? Hours { get; set; }
    }
}

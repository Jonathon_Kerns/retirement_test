﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    [Table("PS_EARNINGS_TBL")]
    public partial class PsEarningsTbl
    {
        [Required]
        [Column("ERNCD")]
        [StringLength(3)]
        public string Erncd { get; set; }
        [Column("EFFDT", TypeName = "datetime")]
        public DateTime Effdt { get; set; }
        [Required]
        [Column("EFF_STATUS")]
        [StringLength(1)]
        public string EffStatus { get; set; }
        [Required]
        [Column("DESCR")]
        [StringLength(30)]
        public string Descr { get; set; }
        [Required]
        [Column("DESCRSHORT")]
        [StringLength(10)]
        public string Descrshort { get; set; }
        [Column("ERN_SEQUENCE")]
        public short ErnSequence { get; set; }
        [Required]
        [Column("MAINTAIN_BALANCES")]
        [StringLength(1)]
        public string MaintainBalances { get; set; }
        [Required]
        [Column("BUDGET_EFFECT")]
        [StringLength(1)]
        public string BudgetEffect { get; set; }
        [Required]
        [Column("ALLOW_EMPLTYPE")]
        [StringLength(1)]
        public string AllowEmpltype { get; set; }
        [Required]
        [Column("PAYMENT_TYPE")]
        [StringLength(1)]
        public string PaymentType { get; set; }
        [Column("HRLY_RT_MAXIMUM", TypeName = "decimal(18, 6)")]
        public decimal HrlyRtMaximum { get; set; }
        [Column("PERUNIT_OVR_RT", TypeName = "decimal(18, 6)")]
        public decimal PerunitOvrRt { get; set; }
        [Column("EARN_FLAT_AMT", TypeName = "decimal(10, 2)")]
        public decimal EarnFlatAmt { get; set; }
        [Required]
        [Column("ADD_GROSS")]
        [StringLength(1)]
        public string AddGross { get; set; }
        [Required]
        [Column("SUBJECT_FWT")]
        [StringLength(1)]
        public string SubjectFwt { get; set; }
        [Required]
        [Column("SUBJECT_FICA")]
        [StringLength(1)]
        public string SubjectFica { get; set; }
        [Required]
        [Column("SUBJECT_FUT")]
        [StringLength(1)]
        public string SubjectFut { get; set; }
        [Required]
        [Column("SUBJECT_CIT")]
        [StringLength(1)]
        public string SubjectCit { get; set; }
        [Required]
        [Column("SUBJECT_CUI")]
        [StringLength(1)]
        public string SubjectCui { get; set; }
        [Required]
        [Column("SUBJECT_CUI_HOURS")]
        [StringLength(1)]
        public string SubjectCuiHours { get; set; }
        [Required]
        [Column("SUBJECT_CPP")]
        [StringLength(1)]
        public string SubjectCpp { get; set; }
        [Required]
        [Column("SUBJECT_QIT")]
        [StringLength(1)]
        public string SubjectQit { get; set; }
        [Required]
        [Column("SUBJECT_QPP")]
        [StringLength(1)]
        public string SubjectQpp { get; set; }
        [Required]
        [Column("SUBJECT_TRUE_T4GRS")]
        [StringLength(1)]
        public string SubjectTrueT4grs { get; set; }
        [Required]
        [Column("SUBJECT_TRUE_RVGRS")]
        [StringLength(1)]
        public string SubjectTrueRvgrs { get; set; }
        [Required]
        [Column("SUBJECT_PAY_TAX")]
        [StringLength(1)]
        public string SubjectPayTax { get; set; }
        [Required]
        [Column("SUBJECT_REG")]
        [StringLength(1)]
        public string SubjectReg { get; set; }
        [Required]
        [Column("WITHHOLD_FWT")]
        [StringLength(1)]
        public string WithholdFwt { get; set; }
        [Required]
        [Column("HRS_ONLY")]
        [StringLength(1)]
        public string HrsOnly { get; set; }
        [Required]
        [Column("SHIFT_DIFF_ELIG")]
        [StringLength(1)]
        public string ShiftDiffElig { get; set; }
        [Required]
        [Column("TAX_GRS_COMPNT")]
        [StringLength(5)]
        public string TaxGrsCompnt { get; set; }
        [Required]
        [Column("SPEC_CALC_RTN")]
        [StringLength(1)]
        public string SpecCalcRtn { get; set; }
        [Column("FACTOR_MULT", TypeName = "decimal(9, 4)")]
        public decimal FactorMult { get; set; }
        [Column("FACTOR_RATE_ADJ", TypeName = "decimal(9, 4)")]
        public decimal FactorRateAdj { get; set; }
        [Column("FACTOR_HRS_ADJ", TypeName = "decimal(9, 4)")]
        public decimal FactorHrsAdj { get; set; }
        [Column("FACTOR_ERN_ADJ", TypeName = "decimal(9, 4)")]
        public decimal FactorErnAdj { get; set; }
        [Required]
        [Column("GL_EXPENSE")]
        [StringLength(35)]
        public string GlExpense { get; set; }
        [Required]
        [Column("SUBTRACT_EARNS")]
        [StringLength(1)]
        public string SubtractEarns { get; set; }
        [Required]
        [Column("DEDCD_PAYBACK")]
        [StringLength(6)]
        public string DedcdPayback { get; set; }
        [Required]
        [Column("TAX_METHOD")]
        [StringLength(1)]
        public string TaxMethod { get; set; }
        [Column("EARN_YTD_MAX", TypeName = "decimal(10, 2)")]
        public decimal EarnYtdMax { get; set; }
        [Required]
        [Column("BASED_ON_TYPE")]
        [StringLength(1)]
        public string BasedOnType { get; set; }
        [Required]
        [Column("BASED_ON_ERNCD")]
        [StringLength(3)]
        public string BasedOnErncd { get; set; }
        [Required]
        [Column("BASED_ON_ACC_ERNCD")]
        [StringLength(3)]
        public string BasedOnAccErncd { get; set; }
        [Required]
        [Column("AMT_OR_HOURS")]
        [StringLength(1)]
        public string AmtOrHours { get; set; }
        [Required]
        [Column("ELIG_FOR_RETROPAY")]
        [StringLength(1)]
        public string EligForRetropay { get; set; }
        [Required]
        [Column("USED_TO_PAY_RETRO")]
        [StringLength(1)]
        public string UsedToPayRetro { get; set; }
        [Required]
        [Column("EFFECT_ON_FLSA")]
        [StringLength(1)]
        public string EffectOnFlsa { get; set; }
        [Required]
        [Column("FLSA_CATEGORY")]
        [StringLength(1)]
        public string FlsaCategory { get; set; }
        [Required]
        [Column("REG_PAY_INCLUDED")]
        [StringLength(1)]
        public string RegPayIncluded { get; set; }
        [Required]
        [Column("TIPS_CATEGORY")]
        [StringLength(1)]
        public string TipsCategory { get; set; }
        [Required]
        [Column("ADD_DE")]
        [StringLength(1)]
        public string AddDe { get; set; }
        [Required]
        [Column("SUBJECT_T4A")]
        [StringLength(1)]
        public string SubjectT4a { get; set; }
        [Required]
        [Column("SUBJECT_RV2")]
        [StringLength(1)]
        public string SubjectRv2 { get; set; }
        [Required]
        [Column("GVT_BENEFITS_RATE")]
        [StringLength(1)]
        public string GvtBenefitsRate { get; set; }
        [Required]
        [Column("GVT_CPDF_ERNCD")]
        [StringLength(1)]
        public string GvtCpdfErncd { get; set; }
        [Required]
        [Column("GVT_OTH_PAY")]
        [StringLength(1)]
        public string GvtOthPay { get; set; }
        [Required]
        [Column("GVT_PAY_CAP")]
        [StringLength(1)]
        public string GvtPayCap { get; set; }
        [Required]
        [Column("GVT_PREM_PAY")]
        [StringLength(1)]
        public string GvtPremPay { get; set; }
        [Required]
        [Column("GVT_OT_PAY_IND")]
        [StringLength(1)]
        public string GvtOtPayInd { get; set; }
        [Required]
        [Column("GVT_ADD_TO_SF50_52")]
        [StringLength(1)]
        public string GvtAddToSf5052 { get; set; }
        [Required]
        [Column("GVT_INCLUDE_LOC")]
        [StringLength(1)]
        public string GvtIncludeLoc { get; set; }
        [Required]
        [Column("GVT_IRR_REPORTABLE")]
        [StringLength(1)]
        public string GvtIrrReportable { get; set; }
        [Required]
        [Column("GVT_IRR_LWOP")]
        [StringLength(1)]
        public string GvtIrrLwop { get; set; }
        [Required]
        [Column("GVT_SF113A_LUMPSUM")]
        [StringLength(1)]
        public string GvtSf113aLumpsum { get; set; }
        [Required]
        [Column("GVT_SF113A_WAGES")]
        [StringLength(1)]
        public string GvtSf113aWages { get; set; }
        [Required]
        [Column("GVT_FEFFLA")]
        [StringLength(1)]
        public string GvtFeffla { get; set; }
        [Required]
        [Column("GVT_FMLA")]
        [StringLength(1)]
        public string GvtFmla { get; set; }
        [Required]
        [Column("GVT_LV_EARN_TYPE")]
        [StringLength(1)]
        public string GvtLvEarnType { get; set; }
        [Required]
        [Column("INCOME_CD_1042")]
        [StringLength(2)]
        public string IncomeCd1042 { get; set; }
        [Required]
        [Column("PERMANENCY_NLD")]
        [StringLength(1)]
        public string PermanencyNld { get; set; }
        [Required]
        [Column("TAX_CLASS_NLD")]
        [StringLength(1)]
        public string TaxClassNld { get; set; }
        [Required]
        [Column("HRS_DIST_SW")]
        [StringLength(1)]
        public string HrsDistSw { get; set; }
        [Required]
        [Column("HP_ADMINSTIP_FLAG")]
        [StringLength(1)]
        public string HpAdminstipFlag { get; set; }
        [Required]
        [Column("SUBJECT_QPIP")]
        [StringLength(1)]
        public string SubjectQpip { get; set; }
        [Required]
        [Column("PNA_USE_SGL_EMPL")]
        [StringLength(1)]
        public string PnaUseSglEmpl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    [Table("PS_COT_PENSION_TBL")]
    public partial class PsCotPensionTbl
    {
        [Required]
        [Column("EMPLID")]
        [StringLength(11)]
        public string Emplid { get; set; }
        [Column("EFFDT", TypeName = "datetime")]
        public DateTime Effdt { get; set; }
        [Column("EFFSEQ")]
        public short Effseq { get; set; }
        [Required]
        [Column("RUNCNTLID")]
        [StringLength(30)]
        public string Runcntlid { get; set; }
        [Column("LTD_POL_PT", TypeName = "decimal(12, 2)")]
        public decimal LtdPolPt { get; set; }
        [Column("LTD_POL_AT", TypeName = "decimal(12, 2)")]
        public decimal LtdPolAt { get; set; }
        [Column("LTD_FIRE_PT", TypeName = "decimal(12, 2)")]
        public decimal LtdFirePt { get; set; }
        [Column("LTD_FIRE_AT", TypeName = "decimal(12, 2)")]
        public decimal LtdFireAt { get; set; }
        [Column("LTD_GEN_PT", TypeName = "decimal(12, 2)")]
        public decimal LtdGenPt { get; set; }
        [Column("LTD_GEN_AT", TypeName = "decimal(12, 2)")]
        public decimal LtdGenAt { get; set; }
        [Column("LTD_INT", TypeName = "decimal(12, 2)")]
        public decimal LtdInt { get; set; }
        [Column("LTD_PT_REFUND", TypeName = "decimal(12, 2)")]
        public decimal LtdPtRefund { get; set; }
        [Column("LTD_AT_REFUND", TypeName = "decimal(12, 2)")]
        public decimal LtdAtRefund { get; set; }
        [Column("LTD_INT_REFUND", TypeName = "decimal(12, 2)")]
        public decimal LtdIntRefund { get; set; }
        [Column("LTD_PT_ROLL", TypeName = "decimal(12, 2)")]
        public decimal LtdPtRoll { get; set; }
        [Column("LTD_INT_ROLL", TypeName = "decimal(12, 2)")]
        public decimal LtdIntRoll { get; set; }
        [Column("FYTD_POL_PT", TypeName = "decimal(10, 2)")]
        public decimal FytdPolPt { get; set; }
        [Column("FYTD_POL_AT_BB", TypeName = "decimal(10, 2)")]
        public decimal FytdPolAtBb { get; set; }
        [Column("FYTD_FIRE_PT", TypeName = "decimal(10, 2)")]
        public decimal FytdFirePt { get; set; }
        [Column("FYTD_FIRE_AT_BB", TypeName = "decimal(10, 2)")]
        public decimal FytdFireAtBb { get; set; }
        [Column("FYTD_GEN_PT", TypeName = "decimal(10, 2)")]
        public decimal FytdGenPt { get; set; }
        [Column("FYTD_GEN_AT_BB", TypeName = "decimal(10, 2)")]
        public decimal FytdGenAtBb { get; set; }
        [Column("POL_SS_PENS", TypeName = "decimal(10, 2)")]
        public decimal PolSsPens { get; set; }
        [Column("FIRE_SS_PENS", TypeName = "decimal(10, 2)")]
        public decimal FireSsPens { get; set; }
        [Column("GEN_SS_PENS", TypeName = "decimal(10, 2)")]
        public decimal GenSsPens { get; set; }
        [Column("PENS_ENTRY_DT", TypeName = "datetime")]
        public DateTime? PensEntryDt { get; set; }
        [Column("COLA_ELIG_DT", TypeName = "datetime")]
        public DateTime? ColaEligDt { get; set; }
        [Required]
        [Column("RETIRE_FLAG")]
        [StringLength(1)]
        public string RetireFlag { get; set; }
        [Column("DEATH_BEN_FLS", TypeName = "decimal(12, 2)")]
        public decimal DeathBenFls { get; set; }
        [Column("DEATH_BEN_CALC", TypeName = "decimal(10, 2)")]
        public decimal DeathBenCalc { get; set; }
        [Column("MONTH_COMPRATE", TypeName = "decimal(8, 2)")]
        public decimal MonthComprate { get; set; }
        [Required]
        [Column("PRIOR_SERVICE")]
        [StringLength(1)]
        public string PriorService { get; set; }
        [Required]
        [Column("COT_ADJ_REASON")]
        [StringLength(100)]
        public string CotAdjReason { get; set; }
        [Required]
        [Column("COT_BUYBACK_CD")]
        [StringLength(6)]
        public string CotBuybackCd { get; set; }
        [Column("ELIG_DATE", TypeName = "datetime")]
        public DateTime? EligDate { get; set; }
        [Column("COT_CITY_PED", TypeName = "datetime")]
        public DateTime? CotCityPed { get; set; }
        [Required]
        [Column("COT_OPT_SELECTED")]
        [StringLength(4)]
        public string CotOptSelected { get; set; }
    }
}

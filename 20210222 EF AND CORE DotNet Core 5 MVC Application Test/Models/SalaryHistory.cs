﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    [Table("SalaryHistory")]
    public partial class SalaryHistory
    {
        [Column("EMPLID")]
        [StringLength(50)]
        public string Emplid { get; set; }
        [Column("SSN")]
        [StringLength(50)]
        public string Ssn { get; set; }
        [StringLength(25)]
        public string Name { get; set; }
        public int? Year { get; set; }
        [Column(TypeName = "decimal(19, 4)")]
        public decimal? Amount { get; set; }
    }
}

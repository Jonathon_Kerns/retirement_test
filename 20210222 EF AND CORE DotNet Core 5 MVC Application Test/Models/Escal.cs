﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models
{
    [Keyless]
    public partial class Escal
    {
        [Column("EMPLID")]
        [StringLength(50)]
        public string Emplid { get; set; }
        [Column("YEAR")]
        public int? Year { get; set; }
        [Column("NAME")]
        [StringLength(25)]
        public string Name { get; set; }
        [Column("SUM_AMOUNT", TypeName = "decimal(38, 4)")]
        public decimal? SumAmount { get; set; }
    }
}

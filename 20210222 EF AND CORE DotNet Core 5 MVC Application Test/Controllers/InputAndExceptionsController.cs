﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Contexts;
using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models;

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Controllers
{
    public class InputAndExceptionsController : Controller
    {
        private readonly CTHCPRDContext _context;

        public InputAndExceptionsController(CTHCPRDContext context)
        {
            _context = context;
        }

        // GET: InputAndExceptions
        public async Task<IActionResult> Index()
        {
            return View(await _context.InputAndExceptions.ToListAsync());
        }

        // GET: InputAndExceptions/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inputAndException = await _context.InputAndExceptions
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (inputAndException == null)
            {
                return NotFound();
            }

            return View(inputAndException);
        }

        // GET: InputAndExceptions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InputAndExceptions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Emplid,Name,EarnCode,Descr,YrMth,Earnings,Hours")] InputAndException inputAndException)
        {
            if (ModelState.IsValid)
            {
                _context.Add(inputAndException);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(inputAndException);
        }

        // GET: InputAndExceptions/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inputAndException = await _context.InputAndExceptions.FindAsync(id);
            if (inputAndException == null)
            {
                return NotFound();
            }
            return View(inputAndException);
        }

        // POST: InputAndExceptions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Emplid,Name,EarnCode,Descr,YrMth,Earnings,Hours")] InputAndException inputAndException)
        {
            if (id != inputAndException.Emplid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inputAndException);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InputAndExceptionExists(inputAndException.Emplid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(inputAndException);
        }

        // GET: InputAndExceptions/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inputAndException = await _context.InputAndExceptions
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (inputAndException == null)
            {
                return NotFound();
            }

            return View(inputAndException);
        }

        // POST: InputAndExceptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var inputAndException = await _context.InputAndExceptions.FindAsync(id);
            _context.InputAndExceptions.Remove(inputAndException);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InputAndExceptionExists(string id)
        {
            return _context.InputAndExceptions.Any(e => e.Emplid == id);
        }
    }
}

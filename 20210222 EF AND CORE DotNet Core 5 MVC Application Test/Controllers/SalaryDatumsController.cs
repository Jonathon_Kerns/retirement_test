﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Contexts;
using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models;

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Controllers
{
    //[BindProperties(SupportsGet = true)]
    public class SalaryDatumsController : Controller
    {
        private readonly CTHCPRDContext _context;

        public SalaryDatumsController(CTHCPRDContext context)
        {
            _context = context;
        }

        
        //[HttpPost("{employee_id}")]
        //[HttpPost("{effective_date}")]
        // GET: SalaryDatums
        //[AcceptVerbs("POST")]
        public async Task<IActionResult> Index(string emplid, string effective_Date)
        {


           // var salaryDatum = "";
            
            var salaryDatum = _context.SalaryData.Where(e => e.Emplid == emplid)
                .Where(e => Convert.ToInt32(e.EffectiveDate) <= Convert.ToInt32(effective_Date))
                .OrderBy(e => e.EFFDT)
                .AsNoTracking();
            


            /*
            if (salaryDatum == null)
            {
                return NotFound();
            }
            */

            return View(await salaryDatum.ToListAsync());
            //return View(salaryDatum);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Test(string emplid, string effectiveDate)
        {


            // var salaryDatum = "";

            var salaryDatum = _context.SalaryData.Where(e => e.Emplid == emplid)
                .Where(e => Convert.ToInt32(e.EffectiveDate) <= Convert.ToInt32(effectiveDate))
                .OrderBy(e => e.EFFDT)
                .AsNoTracking();



            /*
            if (salaryDatum == null)
            {
                return NotFound();
            }
            */

            return View(await salaryDatum.ToListAsync());
            //return View(salaryDatum);


        }

        // GET: SalaryDatums/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salaryDatum = await _context.SalaryData
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (salaryDatum == null)
            {
                return NotFound();
            }

            return View(salaryDatum);
        }

        // GET: SalaryDatums/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SalaryDatums/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Emplid,Year,Month,TotalSsp,YearTotal,GenSsp,FireSsp,PoliceSsp")] SalaryDatum salaryDatum)
        {
            if (ModelState.IsValid)
            {
                _context.Add(salaryDatum);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(salaryDatum);
        }

        // GET: SalaryDatums/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salaryDatum = await _context.SalaryData.FindAsync(id);
            if (salaryDatum == null)
            {
                return NotFound();
            }
            return View(salaryDatum);
        }

        // POST: SalaryDatums/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Emplid,Year,Month,TotalSsp,YearTotal,GenSsp,FireSsp,PoliceSsp")] SalaryDatum salaryDatum)
        {
            if (id != salaryDatum.Emplid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(salaryDatum);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SalaryDatumExists(salaryDatum.Emplid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(salaryDatum);
        }

        // GET: SalaryDatums/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salaryDatum = await _context.SalaryData
                .FirstOrDefaultAsync(m => m.Emplid == id);
            if (salaryDatum == null)
            {
                return NotFound();
            }

            return View(salaryDatum);
        }

        // POST: SalaryDatums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var salaryDatum = await _context.SalaryData.FindAsync(id);
            _context.SalaryData.Remove(salaryDatum);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SalaryDatumExists(string id)
        {
            return _context.SalaryData.Any(e => e.Emplid == id);
        }
    }
}

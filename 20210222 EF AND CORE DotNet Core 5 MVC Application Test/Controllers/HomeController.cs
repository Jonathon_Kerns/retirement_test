﻿using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Contexts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace _20210222_EF_AND_CORE_DotNet_Core_5_MVC_Application_Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly CTHCPRDContext _context;

        public HomeController(CTHCPRDContext context)
        {
            _context = context;
        }

        /*
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        */

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult GetSalaries()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Test(string emplid, string effectiveDate)
        
        {


            // var salaryDatum = "";

            var salaryDatum = _context.SalaryData.Where(e => e.Emplid == emplid)
                .Where(e => Convert.ToInt32(e.EffectiveDate) <= Convert.ToInt32(effectiveDate))
                .OrderBy(e => e.EFFDT)
                .AsNoTracking();



            /*
            if (salaryDatum == null)
            {
                return NotFound();
            }
            */

            return View(await salaryDatum.ToListAsync());
            //return View(salaryDatum);


        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
